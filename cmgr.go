// vi: ts=4 sw=4:
/* 
 Mnemonic:	cmgr.go

 Date:		15 November 2009 
 Author: 	E. Scott Daniels

 Mods:		24 Jan 2014 -- added doc and error checking.
			13 Apr 2017 -- Added faster udp write.
*/

/*
	Creates and manages a connetion environment that allows the user to establish TCP connections
	(outgoing) and listens/accepts connection requests from remote processes. 

	The connection manager object is created with NewManager which accepts a listen port and a 
	channel on which Sess_data objects are written.  The listener is invoked and when a connection is 
	accepted a connection reader is started and a ST_NEW Sess_data object is written on the user's
	channel. The reader sends all data via the channel (ST_DATA) and if the session is disconnected
	a Sess_data object with the ST_DISC state is written and the connection is cleaned up (no need for user to 
	invoke the Close function in a disconnect case. 

	When a user establishes a connection the remote IP address and a session id (name) are supplied along 
	with a communication channel. The channel may be the same channel supplied when the connection manager 
	object was created or it may be a different channel.  

	Data received from either a UDP listener, or on a connected TCP session is bundled into a Sess_data 
	struct and placed onto the appropriate channel.  The struct contains, in addition to the received
	buffer, the ID of the session that can be used on a generic Write command to, the current state of 
	the session (ST_ constants), and a string indicating some useful (humanised) data about the session. 
*/		
package cmgr

import (
	"fmt"
	"net"
	"os"
)

const (
					// state describing the session data returned on the user's channel
	ST_NEW = iota	// new connection
	ST_DATA			// data received 
	ST_DISC			// disconnected connection
	ST_ACCEPTED		// session has been accepted
)

const(						// connection states
	ST_CLOSING int = 1 		// close already in progress
)

const (
	LISTENER string = "l0" 		// our magic session id for the default litener
)

type Cmgr struct {						// main session manager class
	clist	map[string] *connection 	// tcp connections, also tracks udp listeners
	llist	map[string] net.Listener 	// tcp listeners
	lcount	int 						// tcp listener count for id string generation
	ucount	int 						// udp 'listener' count for id string
}

/*
	Data that is returned on the channel to the user. 
*/
type Sess_data struct {
	Buf		[]byte		// actual data
	Id		string		// identify src for user that receives data from multiple connections
	From	string		// message source address
	State	int			// ST_ constants indicating the session state
	Data	string		// maybe useful (humanised) data about the session or message; generally empty for data.
	sender	*connection		// enables the data block to be used as a writer
}


type connection struct {			// track specifics for a single 'connection'
	id			string 				// our assigned id (hashed)
	conn		net.Conn 			// connection interface
	uconn		*net.UDPConn 		// UDP socket
	data2usr	chan *Sess_data 	// channel to send data from this conn to user
	bytes_in	int64
	bytes_out	int64
	state		int 				// current state
}

/* -------------- private ------------------------------------------------------- */
// listen and accept connections
func (this *Cmgr) listener(  l net.Listener, data2usr chan *Sess_data ) {
	var n 	int = 0
	
	for {
		conn, err := l.Accept( )
		if err == nil {
			n += 1
			conn_data := new( connection )
			conn_data.id = fmt.Sprintf( "a%d", n )
			conn_data.conn = conn
			conn_data.data2usr = data2usr
			this.clist[conn_data.id] = conn_data 		// hash for write to session

			sdp := new( Sess_data ) 			// create and format accept msg back to user
			sdp.Id = conn_data.id
			sdp.From = conn.RemoteAddr().String()
			sdp.State = ST_ACCEPTED
			sdp.Data = fmt.Sprintf( "connection [%s] accepted from: %s", conn_data.id, sdp.From )
			data2usr <- sdp

			go this.conn_reader( conn_data )
		} else {
			return
		}
	}
}

/*
	Create a new data object.
*/
func newdata( buf []byte, id string, state int, from *net.UDPAddr, data string ) (* Sess_data) {
	sdp := new( Sess_data )
	sdp.Buf = buf
	sdp.Id = id
	if from != nil {
		sdp.From = from.String()
	}

	sdp.State = state
	sdp.Data = data

	return sdp
}

/*
	conn_reader reads from sesion and write on the user's channel
	on error (disc) we send the user a nil buffer on the channel and close things up
*/
func (this *Cmgr) conn_reader( cp *connection )   {
	var buf []byte

	buf = make( []byte, 2048 )

	if cp.conn != nil {
		cp.data2usr <- newdata( nil, cp.id, ST_NEW, nil, fmt.Sprintf( "%s", cp.conn.RemoteAddr()) )   // indicate new session 
	}

	for {
		var nread 	int
		var err		error
		var from	*net.UDPAddr = nil 	// packet source if udp 

		if cp.conn != nil {
			nread, err = cp.conn.Read( buf );	
		} else {
			nread, from, err = cp.uconn.ReadFromUDP( buf );	
		}

/*
		--- in original go this logic was needed, though it seems that with the evolution it's not any longer ----
		if err != nil {
			//if e2common( err ) !=  os.EAGAIN {	// we can ignore this if we assume all errors mean close
			if e, ok := err.(os.PathError); ok && e.Error != os.EAGAIN {		//e is null if err isn't os.Error
				cp.data2usr <- newdata( nil, cp.id, ST_DISC, nil, "" ) 	// disco to the user programme	
				cp.data2usr = nil
				this.Close( cp.id ) 		// drop our side and stop
				return
			}
		} else {
			if cp.data2usr != nil {
				cp.bytes_in += int64( nread )
				cp.data2usr <- newdata( buf[0:nread], cp.id, ST_DATA, from, "" )
			}
		}
*/
		if err != nil {					// assume that eagain has been implemented out
			cp.data2usr <- newdata( nil, cp.id, ST_DISC, nil, "" ) 	// disco to the user programme	
			cp.data2usr = nil
			this.Close( cp.id ) 		// drop our side and stop
			return
		}

		if cp.data2usr != nil {							// a nil buffer signals end to caller, so only write if not nil
			cp.bytes_in += int64( nread )
			cp.data2usr <- newdata( buf[0:nread], cp.id, ST_DATA, from, "" )
		}
	}
}

/* ------ public ---------------------------------------------------- */

/*
	Listen starts a TCP listener, allowing the caller to supply type (tcp, tcp4, tcp6) and interface (0.0.0.0 for any)
	then opens and binds to the socket. A goroutine is started to actually do the listening and will
	accept sessions that connect. Generally the listen method will be driven during the construction of 
	a cmgr object, though I user can use this if more than one listen port is required. 

	Returns an ID which identifies the listener, and a boolean set to true if the listener was established
	successfully.
*/
func (this *Cmgr) Listen( kind string, port string,  iface string, data2usr chan *Sess_data ) (string, bool) {
	if port == ""  || port == "0" || data2usr == nil {		// user probably called constructor not wanting a listener
		return "", true
	}

	lid := fmt.Sprintf( "l%d", this.lcount )
	this.lcount += 1

	l, err := net.Listen( kind, fmt.Sprintf( "%s:%s", iface, port ) )
	if err != nil {
		fmt.Fprintf( os.Stderr, "unable to create a listener on port: %s; %s\n", port, err )
		return "", false;	
	}

	this.llist[lid] = l
	go this.listener(  this.llist[lid], data2usr )
	return lid, true
}

/*
	Listen_udp starts a UDP listener which will forward received data back to the application using
	the supplied channel.  The listener ID is returned along with a boolian indication
	of success (true) or failure.
*/
func (this *Cmgr) Listen_udp( port int, data2usr chan *Sess_data ) (string, bool) {
	var addr	net.UDPAddr

	addr.IP = net.IPv4( 0, 0, 0, 0 )
	addr.Port = port
	uconn, err := net.ListenUDP( "udp",  &addr )
	if err != nil {
		fmt.Fprintf( os.Stderr, "unable to create a udp listener on port: %d; %s\n", port, err )
		return "", false;	
	}

	uid := fmt.Sprintf( "u%d", this.ucount ) 	// successful bind to port
	this.ucount += 1

	cp := new( connection )
	cp.conn = nil
	cp.uconn = uconn
	cp.data2usr = data2usr 		// session data written to the channel
	cp.id = uid 			// user assigned session id
	this.clist[uid] = cp 		// hash for write to session
	go this.conn_reader( cp ) 	// start reader; will discard if data2usr is nil
	
	this.clist[uid] = cp
	return uid, true
}

/*
	Listen_tcp provides a more consistant interface with the Listen_udp name convention and is just
	a wrapper for Listen().
*/
func (this *Cmgr) Listen_tcp( port string, data2usr chan *Sess_data ) (string, bool) {
	return this.Listen( "tcp", port, "0.0.0.0", data2usr )
}

/*
	Lists the current statistics about connections to the standard output device.
*/
func (this *Cmgr) List_stats(  ) {
	var ucount int = 0 		// count of udp 'listeners' to dec conn count by

	fmt.Fprintf( os.Stderr,  "%d tcp listeners:\n", len( this.llist ) ) 		// tcp listeners
	for l := range this.llist {
		fmt.Printf( "\t%s on %s\n", l, this.llist[l].Addr().String()  )
	}

	for cname := range this.clist {				// udp listeners
		cp := this.clist[cname]
		if cp.uconn != nil {
			ucount += 1
			fmt.Printf( "\t%s UDP on %s  %5d %5d\n", cp.id,  cp.uconn.LocalAddr().String(), cp.bytes_in, cp.bytes_out )
		}
	}

	fmt.Printf( "%d tcp connections:\n", len( this.clist ) - ucount ) 		// established tcp connections
	for cname := range this.clist {
		cp := this.clist[cname]
		if cp.conn != nil {
			fmt.Printf( "\t%s -> %s %5d %5d\n", cp.id, cp.conn.RemoteAddr(), cp.bytes_in, cp.bytes_out )
		}
	}

}

/*
	Connect establishes a connection to the target process (ip:port) and starts a reader listening 
	for data on the session.  Any received data will be forwarded to the user application
	via the channel provided. 
*/
func (this *Cmgr) Connect( target string, uid string, data2usr chan *Sess_data ) {
	//var err	os.Error
	var(
		 err error
	)

	cp := new( connection )
	cp.conn, err = net.Dial( "tcp", target )
	if( err != nil ) {
		fmt.Fprintf( os.Stderr, "session_connect: unable to create session to: %s: %s\n", target, err )
	} else {
		cp.data2usr = data2usr 		// session data written to the channel
		cp.id = uid 			// user assigned session id
		this.clist[uid] = cp 		// hash for write to session
		go this.conn_reader( cp ) 	// start reader; will discard if data2usr is nil
	}
}

// ----------- generic write functions allowing writes to a named session ---------------------------------------

/*
	Write_to_all writes the byte array to all active sessions.
*/
func (this *Cmgr) Write_to_all( buf []byte ) ( err error ) {
	var (
		n	int
		nw	int
		offset int
	)

	err = nil

	//if cp, ok := this.clist[id]; ok {
	for _, cp := range this.clist {
		if cp.conn == nil {
			return
		}

		cp.bytes_out += int64( len( buf ) )

		offset = 0
		for n = len( buf ); n >0; {
			nw, err = cp.conn.Write( buf[offset:] ) 			// ignore error assuming that reader will catch and close things up
			if err != nil {
				return err
			}

			offset += nw
			n -= nw;
		}
	}

	return nil
}

/*
	Write writes the byte array to the named connection. 
*/
func (this *Cmgr) Write( id string, buf []byte ) ( err error ) {
	var (
		n	int
		nw	int
	)

	err = nil

	if cp, ok := this.clist[id]; ok {
		if cp.conn == nil {
			return
		}

		cp.bytes_out += int64( len( buf ) )

		for n = len( buf ) ; n >0 ; {
			nw, err = cp.conn.Write( buf ) 	// ignore error assuming that reader will catch and close things up
			n -= nw;
			if err != nil {
				return
			}
		}
	}

	return
}

/*
	Write_n writes n bytes from the byte array to the named session. 
*/
func (this *Cmgr) Write_n( id string, buf []byte, n int ) ( err error ){
	var (
		nw	int
	)

	err = nil

	if cp, ok := this.clist[id]; ok {
		cp.bytes_out += int64( len( buf ) )

		for  ; n >0 ; {
			nw, err = cp.conn.Write( buf ) 	// ignore error assuming that reader will catch and close things up
			n -= nw;
			if err != nil {
				return
			}
		}
	}

	return
}

/*
	Write_udp writes the string to the named session. 
*/
func (this *Cmgr) Write_str( id string, buf string ) ( err error ) {
	return this.Write( id,  []byte( buf ) )
}

/*
	Writes the byte array to the udp address given in 'to'.  The address is expected to be host:port format.
	To is the addr:port to send the data to, and id of the UDP listener that is used for writing.
	Ibuf is one of: array of bytes, string or pointer to string containing the data to write.
*/
func (this *Cmgr) Write_udp( id string, to string, ibuf interface{}) ( err error ) {
	var buf []byte

	err = nil

	switch ibuf.( type ) {
		case []byte:
			buf = ibuf.( []byte )						// already correct, just assert type

		case string:
			buf = []byte( ibuf.( string ) )				// assert type, then convert to bytes array

		case *string:
			buf = []byte( *(ibuf.( *string )) )			// assert type, then convert to bytes array

		default:
			return fmt.Errorf( "unknown buffer type %v is not supported", ibuf )
	}

	if cp, ok := this.clist[id]; ok {
		addr, e := net.ResolveUDPAddr( "udp", to ) 		// parm1 is either udp, udp4 or udp6
		if e != nil {
			fmt.Fprintf( os.Stderr, "write_udp: unable to convert address: %s: %s\n", to, e )
			err = e
			return
		}

		cp.bytes_out += int64( len( buf ) )
		_, err = cp.uconn.WriteToUDP( buf, addr ) 	// ignore error assuming that reader will catch and close things up
	} else {
		err = fmt.Errorf( "unable to find id in table: %s", id )
	}

	return
}

/*
	Write_udp_addr writes the data (ibuf) to the udp address given in 'to'.  The address in to is something that has been resolved
	by net.ResolveUDPAddr() so this should be faster than Write_udp(). Ibuf may be a byte array, string or pointer to string.
*/
func (this *Cmgr) Write_udp_addr( id string, toi interface{}, ibuf interface{}) ( err error ) {
	var buf []byte

	to, ok := toi.( *net.UDPAddr )
	if !ok {
		return fmt.Errorf( "to was not the expected address interface type" )
	}

	err = nil

	switch ibuf.( type ) {
		case []byte:
			buf = ibuf.( []byte )						// already correct, just assert type

		case string:
			buf = []byte( ibuf.( string ) )				// assert type, then convert to bytes array

		case *string:
			buf = []byte( *(ibuf.( *string )) )			// assert type, then convert to bytes array

		default:
			return fmt.Errorf( "unknown buffer type %v is not supported", ibuf )
	}

	if cp, ok := this.clist[id]; ok {
		cp.bytes_out += int64( len( buf ) )
		_, err = cp.uconn.WriteToUDP( buf, to ) 	// ignore error assuming that reader will catch and close things up
	} else {
		err = fmt.Errorf( "unable to find id in table: %s", id )
	}

	return
}

/*
	Xlate_udp_addr allows  a user to xlate an ip:port host:port into a udp address that can be passed to Write_udp_addr
	for faster udp writes.
*/
func Xlate_udp_addr( addr string ) ( xla *net.UDPAddr, e error ) {
	xla, e = net.ResolveUDPAddr( "udp", addr ) 		// parm1 is either udp, udp4 or udp6
	return xla, e
}

// -------------------------- writes to a specific connection --------------------------------------------------------

/*
	These functions allow a 'direct' reply based on a message that was written to the user's channel
*/

/*
	Write sends the contents of buf (bytes) to the process that sent the data represented by Sess_data
*/
func (this *Sess_data) Write( buf []byte ) ( err error ) {
	var (
		n	int				// number to write
		nw	int				// number written
	)

	err = nil

	this.sender.bytes_out += int64( len( buf ) )
	
	for n = len( buf ); n > 0; {
		nw, err = this.sender.conn.Write( buf ) 			// write until we wrote all	
		n -= nw;
		if err != nil {
			return
		}
	}

	return
}

/*
	Write_n sends n bytes to the process that sent the data represented by Sess_data
*/
func (this *Sess_data ) Write_n( buf []byte, n int ) ( err error ) {
	var (
		nw int
	)
	
	err = nil

	this.sender.bytes_out += int64( n )
	for ; n > 0; {
		nw, err = this.sender.conn.Write( buf[0:n] ) 	// ignore error assuming that reader will catch and close things up

		n -= nw
		if err != nil {
			return
		}
	}

	return
}

/*
	Write_str sends the string to the process that sent the data represented by Sess_data
*/
func (this *Sess_data ) Write_str( buf string ) ( err error ) {
	return this.Write( []byte( buf ) )
}


// -------------------------------------------------------------------------------------------------------------------


/*
	Close does what ever is needed to terminate the connection represented by id.
*/
func (this *Cmgr) Close( id string ) {
	
	sess, ok := this.clist[id] 		// map id to the session data
	if ok {
		if( sess.state != ST_CLOSING ) { // if close called, read will call us when it popps; in case we are preempted 
			sess.state = ST_CLOSING
			_ = sess.conn.Close( )
			sess.conn = nil
			delete( this.clist, id )
		}

		return
	}

	ls, ok := this.llist[id] 			// listener
	if ok {
		ls.Close( )
		//this.llist[id] = nil, false
		delete( this.llist, id )
	}
}

/*
	NewManager creates a new connection manager object. If the port is greater than zero, a TCP listener will be started on the port
	and any data received by sessions connected to the port will be sent to the user application using the channel 
	provided.   If port is <= 0, or an empty string, then no listener is opened by default; a call to either the
	udp listen or tcp listen function can be made to start a listener and provide a channel.

	If the listener cannot be established a nil object is returned.
	
*/
func NewManager( port string, data2usr chan *Sess_data  ) *Cmgr {
	this := new( Cmgr )
	this.clist = make( map[string] *connection ) 	// must allocate the maps first
	this.llist = make( map[string] net.Listener );	
	this.lcount = 0

	
	_, ok := this.Listen_tcp( port, data2usr ) 		// if port is "" or "0", Listen will ignore and return ok
	if  !ok {
		fmt.Fprintf( os.Stderr, "unable to initialise session manager\n" )
		return nil
	}
	
	return this
}

