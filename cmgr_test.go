/*
	Abstract:	Tests for the cmgr package.

	Date:		18 Janurary 2022
	Author: 	E. Scott Daniels
*/
package cmgr

import (
	"testing"
)

/*
	Basic test; needs to be expanded
*/
func TestCmgr( t *testing.T ) {
	port := "4567"
	ch := make( chan *Sess_data, 10 )
	m := NewManager( port, ch )
	if m == nil {
		t.Fail()
	}
}
